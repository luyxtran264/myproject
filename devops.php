<?php
/*
 * Auto deploy source code after commit source code to server
 * Ref: http://jondavidjohn.com/git-pull-from-a-php-script-not-so-simple/
 * Requirement:
 * 1. Use `which` to determine absolute path of git
 * 2. Set User/Group permission:
 * - nginx => www-data:www-data
 * - apache => www:www
 */
$dir = '/var/www/html/viettransfer.com';
$cd = shell_exec('cd ' . $dir);

echo "<h1>Devops</h1>";
echo "<h2>Git pull</h2>";
echo "<pre>Pulling ... Please wait !</pre>";

ob_flush();
flush();

/* Run git pull */
$output = shell_exec('/usr/bin/git pull 2>&1');
echo "<pre>Output: </pre>";
echo "<pre>$output</pre>";

ob_end_flush();
?>