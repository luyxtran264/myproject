<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //

// ** MySQL settings - You can get this info from your web host ** //
$db_file = dirname(__FILE__) . '/mysql.conf';
if(file_exists($db_file)){
    $str = trim(file_get_contents($db_file));
    $url = parse_url($str);
}else{
    $db_url_key = 'VIETTRANSFER_DATABASE_URL';
    $db_url_key_alternative = 'VIETTRANSFER_CLEARDB_DATABASE_URL';
    $url = parse_url(getenv($db_url_key) ? getenv($db_url_key) : getenv($db_url_key_alternative));
}

if ($url && array_key_exists('pass', $url)){
    /** Production ENV **/

    /** The name of the database for WordPress */
    define('DB_NAME', trim($url['path'], '/'));

    /** MySQL database username */
    define('DB_USER', $url['user']);

    /** MySQL database password */
    define('DB_PASSWORD', $url['pass']);

    /** MySQL hostname */
    define('DB_HOST', $url['host']);

    /** Database Charset to use in creating database tables. */
    define('DB_CHARSET', 'utf8');

    /** The Database Collate type. Don't change this if in doubt. */
    define('DB_COLLATE', '');
}else{
    /** Development ENV **/
    /** Developer can set configuration here **/

    /** The name of the database for WordPress */
    define('DB_NAME', 'wordpress');

    /** MySQL database username */
    define('DB_USER', 'root');

    /** MySQL database password */
    define('DB_PASSWORD', '123456');

    /** MySQL hostname */
    define('DB_HOST', 'localhost');

    /** Database Charset to use in creating database tables. */
    define('DB_CHARSET', 'utf8');

    /** The Database Collate type. Don't change this if in doubt. */
    define('DB_COLLATE', '');
}
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Er DV)>g<,giV,~HHW|s{{m[i|HCf-tS@{872/Ks2CEX|&},%rlmVL|/rD!uO>Wf');
define('SECURE_AUTH_KEY',  'E JDj-qWzK_,{2SmNcLH%Rd`g:bLw:ZBNLhccV<G4Jt)KRO!|=E{a+mgCcQ}gA{j');
define('LOGGED_IN_KEY',    '++OcTL`Qn5Dg{F N=Lcn=ac-<9Djs3z^[7p!C=a;mrt j^U95?c>^UQV>1/fjIaW');
define('NONCE_KEY',        ':&S|iuNp[5`$smr;%@!+Q%5WHmG [/9MEwBN @zd.`OU|ik+K0Z_CjJS)t@&a|?E');
define('AUTH_SALT',        '6!?.YUA^^4_0M0)6lI87(W:~W_ )s1q*B?ZfC2}=nL^n4At8:p48G`XA3{=?qXIy');
define('SECURE_AUTH_SALT', 'HS^BUJTvL]MkD^xW+F+h])DVNshZ^+a[3~h/uGKO;A|?=S_)&a_hWW<1u *_),|f');
define('LOGGED_IN_SALT',   'K?RB&WH.^O#LTog{*YY8*o>jEyMEnOT,Lvv`7*=}a;@Z-0D#/fLVbC*>BB]m(Mr3');
define('NONCE_SALT',       '^#[4Czgi2e(D<0(~i|Vd+(cOaXqtNLc;8dp<Bs8YTCQ:[)~;&ww.J9p# Q[A$Q47');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
